//
//  UIViewController+Extended.swift
//  BackendlessExample
//
//  Created by Alexander Kurbanov on 4/11/16.
//  Copyright © 2016 Alexander Kurbanov. All rights reserved.
//

import Foundation

extension UIViewController {
    func showAlert(message:String) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
}