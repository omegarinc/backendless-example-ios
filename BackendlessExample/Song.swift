//
//  Song.swift
//  BackendlessExample
//
//  Created by Alexander Kurbanov on 3/22/16.
//  Copyright © 2016 Alexander Kurbanov. All rights reserved.
//

import Foundation

class Song: BackendlessEntity {
    var name: String!
    var artist: Artist!
}
