//
//  AddSongViewController.swift
//  BackendlessExample
//
//  Created by Alexander Kurbanov on 3/22/16.
//  Copyright © 2016 Alexander Kurbanov. All rights reserved.
//

import UIKit

class AddSongViewController: UIViewController {
    
    // MARK: Lazily initialized subviews
    
    private lazy var textFieldSongName:UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.placeholder = "Song name"
        textField.backgroundColor = UIColor.lightGrayColor()
        textField.textColor = UIColor.whiteColor()
        textField.autocorrectionType = .No
        return textField
    }()
    
    private lazy var textFieldArtistName:UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.placeholder = "Artist name"
        textField.backgroundColor = UIColor.lightGrayColor()
        textField.textColor = UIColor.whiteColor()
        textField.autocorrectionType = .No
        return textField
    }()
    
    private lazy var buttonAdd:UIButton = {
       let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Add", forState: .Normal)
        button.backgroundColor = UIColor.greenColor()
        button.setTitleColor(UIColor.blackColor(), forState: .Normal)
        button.addTarget(self, action: #selector(onButtonAddPressed), forControlEvents: .TouchUpInside)
        return button
    }()
    
    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.whiteColor()
        self.title = String(AddSongViewController)
        
        self.view.addSubview(self.textFieldSongName)
        self.view.addSubview(self.textFieldArtistName)
        self.view.addSubview(self.buttonAdd)
        let views:[String: AnyObject] = [
                    "textFieldSongName": self.textFieldSongName,
                    "textFieldArtistName": self.textFieldArtistName,
                    "buttonAdd": self.buttonAdd,
                    "topLayout": self.topLayoutGuide,
        ]
        
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[topLayout]-20-[textFieldSongName(50)]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-50-[textFieldSongName]-50-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[textFieldSongName]-20-[textFieldArtistName(50)]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-50-[textFieldArtistName]-50-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[textFieldArtistName]-20-[buttonAdd(50)]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-50-[buttonAdd]-50-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
    }
    
    // MARK: Actions
    
    func onButtonAddPressed() {
        guard let songName = self.textFieldSongName.text where songName.characters.count > 0 else {
            showAlert("Please enter a song name.")
            return
        }
        
        guard let artistName = self.textFieldArtistName.text where artistName.characters.count > 0 else {
            showAlert("Please enter an artist name.")
            return
        }
        
        self.createSong(artistName, songName: songName)
    }
    
    // MARK: Helpers
    
    func createSong(artistName: String, songName: String) {
        self.findOrCreateArtist(artistName) {[weak self] (artist, fault) in
            if (fault != nil) {
                self?.showAlert("Server reported an error: \(fault)")
                return
            }
            
            let song = Song()
            song.name = songName
            song.artist = artist
            let dataStore = Backendless.sharedInstance().data.of(Song.ofClass())
            
            // save object asynchronously
            dataStore.save(
                song,
                response: {[weak self] (result: AnyObject!) -> Void in
                    let obj = result as! Song
                    self?.showAlert("Song has been saved: \(obj.objectId)")
                },
                error: {[weak self] (fault: Fault!) -> Void in
                    self?.showAlert("Server reported an error: \(fault)")
                })
        }
    }
    
    func findOrCreateArtist(name: String, completion: (artist: Artist?, fault: Fault?) -> Void) {
        let dataStore = Backendless.sharedInstance().data.of(Artist.ofClass())
        
        let query = BackendlessDataQuery()
        query.whereClause = "name = '\(name)'"
        
        dataStore.find(query, response: { (collection: BackendlessCollection!) in
            if (collection.data.count > 0) {
                let artist = collection.data[0] as! Artist
                completion(artist: artist, fault: nil)
            } else {
                let artist = Artist()
                artist.name = name
                completion(artist: artist, fault: nil)
            }
        }) {(fault: Fault!) in
            completion(artist: nil, fault: fault)
        }
    }
}
