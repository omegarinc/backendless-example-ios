//
//  SongSelectViewController.swift
//  BackendlessExample
//
//  Created by Alexander Kurbanov on 3/22/16.
//  Copyright © 2016 Alexander Kurbanov. All rights reserved.
//

import UIKit

class SongSelectViewController: UITableViewController {
    
    var songs: [Song]?
    
    // MARK: - View lifecycle
    
     override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.whiteColor()
        self.title = String(SongSelectViewController)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "🎤", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(onButtonAddSongPressed))
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.updateSongs()
    }

    // MARK: - Actions
    
    func onButtonAddSongPressed() {
        let vc = AddSongViewController()
        self.navigationController!.pushViewController(vc, animated: true)
    }
    
    // MARK: - UITableViewDataSource
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let result = self.songs?.count ?? 0
        return result
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let reuseIdentifier = "TableViewCell"
        
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier) ?? UITableViewCell(style: .Default, reuseIdentifier: reuseIdentifier)
        
        let song = self.songs![indexPath.row]
        let text = song.name! + " by " + song.artist!.name
        cell.textLabel!.text = text
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let vc = EditSongViewController()
        vc.song = self.songs![indexPath.row]
        self.navigationController!.pushViewController(vc, animated: true)
    }
    
    // MARK: - Helpers
    
    func updateSongs() {
        let dataStore = Backendless.sharedInstance().data.of(Song.ofClass())
        
        let options = QueryOptions()
        options.addRelated("artist")
        options.sortBy = ["name ASC"];
        
        let query = BackendlessDataQuery()
        query.queryOptions = options
        
        dataStore.find(query, response: {[weak self] (collection: BackendlessCollection!) in
            self?.songs = collection.data as! [Song]!
            self?.tableView.reloadData()
        }) {[weak self] (fault: Fault!) in
            self?.showAlert("Server reported an error: \(fault)")
        }
    }
}
