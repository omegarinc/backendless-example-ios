//
//  Artist.swift
//  BackendlessExample
//
//  Created by Alexander Kurbanov on 3/23/16.
//  Copyright © 2016 Alexander Kurbanov. All rights reserved.
//

import Foundation

class Artist: BackendlessEntity {
    var name: String!
}
