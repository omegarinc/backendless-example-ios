//
//  EditSongViewController.swift
//  BackendlessExample
//
//  Created by Alexander Kurbanov on 4/11/16.
//  Copyright © 2016 Alexander Kurbanov. All rights reserved.
//

import Foundation

class EditSongViewController: UIViewController {
    
    var song: Song!
    
    // MARK: - Lazily initialized subviews
    
    private lazy var textFieldSongName: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.placeholder = "Song name"
        textField.backgroundColor = UIColor.lightGrayColor()
        textField.textColor = UIColor.whiteColor()
        textField.autocorrectionType = .No
        return textField
    }()
    
    private lazy var buttonEdit: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Edit", forState: .Normal)
        button.backgroundColor = UIColor.yellowColor()
        button.setTitleColor(UIColor.blackColor(), forState: .Normal)
        button.addTarget(self, action: #selector(onButtonEditPressed), forControlEvents: .TouchUpInside)
        return button
    }()
    
    private lazy var buttonDelete: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Delete", forState: .Normal)
        button.backgroundColor = UIColor.redColor()
        button.setTitleColor(UIColor.blackColor(), forState: .Normal)
        button.addTarget(self, action: #selector(onButtonDeletePressed), forControlEvents: .TouchUpInside)
        return button
    }()
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.whiteColor()
        self.title = String(EditSongViewController)
        self.textFieldSongName.text = song.name
        
        self.view.addSubview(self.textFieldSongName)
        self.view.addSubview(self.buttonDelete)
        self.view.addSubview(self.buttonEdit)
        let views:[String: AnyObject] = [
            "textFieldSongName": self.textFieldSongName,
            "buttonEdit": self.buttonEdit,
            "buttonDelete": self.buttonDelete,
            "topLayout": self.topLayoutGuide,
            ]
        
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[topLayout]-20-[textFieldSongName(50)]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-50-[textFieldSongName]-50-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))

        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[textFieldSongName]-20-[buttonEdit(50)]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-50-[buttonEdit]-50-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[buttonEdit]-20-[buttonDelete(50)]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-50-[buttonDelete]-50-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
    }
    
    // MARK: - Actions
    
    func onButtonEditPressed() {
        guard let songName = self.textFieldSongName.text where songName.characters.count > 0 else {
            showAlert("Please enter a song name.")
            return
        }
        
        self.editSong(songName)
    }
    
    func onButtonDeletePressed() {
        self.deleteSong()
    }
    
    // MARK: - Helpers
    
    func editSong(newName: String) {
        let dataStore = Backendless.sharedInstance().data.of(Song.ofClass())
        self.song.name = newName
        
        dataStore.save(
            self.song,
            response: {[weak self] (result: AnyObject!) -> Void in
                let obj = result as! Song
                self?.showAlert("Song has been edited: \(obj.objectId)")
            },
            error: {[weak self] (fault: Fault!) -> Void in
                self?.showAlert("Server reported an error: \(fault)")
            })
    }
    
    func deleteSong() {
        let dataStore = Backendless.sharedInstance().data.of(Song.ofClass())
        
        dataStore.remove(self.song, response: {[weak self] (number: NSNumber!) in
            print(number)
            self?.navigationController?.popViewControllerAnimated(true)
        }) {[weak self] (fault: Fault!) in
            self?.showAlert("Server reported an error: \(fault)")
        }
    }
}