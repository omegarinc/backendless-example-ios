//
//  AppDelegate.swift
//  BackendlessExample
//
//  Created by Alexander Kurbanov on 3/22/16.
//  Copyright © 2016 Alexander Kurbanov. All rights reserved.
//

/*
 Login information:
 Email: kurbanov.alex@omega-r.com
 Password: backend
*/

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

//MARK: UIApplicationDelegate
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        // Prepare the main view controller
        let vc = SongSelectViewController()
        let nc = UINavigationController(rootViewController: vc)
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        self.window!.rootViewController = nc
        self.window!.makeKeyAndVisible()
        
        // Init third party libs
        let backendless = Backendless.sharedInstance()
        backendless.initApp("F4E04767-51FF-EBA2-FF2A-D6C05E7F9500", secret:"D81FCFD0-7EEE-3310-FF3C-6C41C0385F00", version:"v1")
        
        return true
    }
}
